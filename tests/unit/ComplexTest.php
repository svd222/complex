<?php

use svd\complex\ComplexNumberAction;
use svd\complex\ComplexNumber;

class ComplexTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected ComplexNumber $number1;

    protected ComplexNumber $number2;

    protected function _before()
    {
        $this->number1 = new ComplexNumber(6, 8);
        $this->number2 = new ComplexNumber(3, 2);
    }

    public function testIsInstaceOf()
    {
        $this->assertInstanceOf(ComplexNumber::class, $this->number1);
    }

    /**
     * @depends testIsInstaceOf
     */
    public function testTypeCasting()
    {
        $this->assertEquals("6 + 8i", (string) $this->number1);
    }

    /**
     * @depends testTypeCasting
     */
    public function testAdd()
    {
        $sum = ComplexNumberAction::add($this->number1, $this->number2);
        $this->assertEquals("9 + 10i", (string) $sum);
    }

    /**
     * @depends testTypeCasting
     */
    public function testDiff()
    {
        $diff = ComplexNumberAction::sub($this->number1, $this->number2);
        $this->assertEquals("3 + 6i", (string) $diff);
    }

    /**
     * @depends testTypeCasting
     */
    public function testMul()
    {
        $mul = ComplexNumberAction::mul($this->number1, $this->number2);
        $this->assertEquals("2 + 36i", (string) $mul);
    }

    /**
     * @depends testTypeCasting
     */
    public function testDiv()
    {
        $div = ComplexNumberAction::div($this->number1, $this->number2);
        $this->assertEqualsWithDelta(2.6, $div->getReal(), 0.02);
        $this->assertEqualsWithDelta(0.92, $div->getImaginary(), 0.03);
    }
}