<?php
namespace svd\complex;

/**
 * Class ComplexNumberAction
 * @package svd\complex
 */
class ComplexNumberAction
{
    /**
     * Adds two complex numbers
     *
     * @param ComplexNumber $number1
     * @param ComplexNumber $number2
     * @return ComplexNumber
     */
    public static function add(ComplexNumber $number1, ComplexNumber $number2): ComplexNumber
    {
        $r = $number1->getReal() + $number2->getReal();
        $y = $number1->getImaginary() + $number2->getImaginary();
        return new ComplexNumber($r, $y);
    }

    /**
     * Subtracts two complex numbers
     *
     * @param ComplexNumber $number1
     * @param ComplexNumber $number2
     * @return ComplexNumber
     */
    public static function sub(ComplexNumber $number1, ComplexNumber $number2): ComplexNumber
    {
        $r = $number1->getReal() - $number2->getReal();
        $y = $number1->getImaginary() - $number2->getImaginary();
        return new ComplexNumber($r, $y);
    }

    /**
     * Multiplies two complex numbers
     *
     * @param ComplexNumber $number1
     * @param ComplexNumber $number2
     * @return ComplexNumber
     */
    public static function mul(ComplexNumber $number1, ComplexNumber $number2): ComplexNumber
    {
        $r = $number1->getReal() * $number2->getReal() - $number1->getImaginary() * $number2->getImaginary();
        $y = $number1->getReal() * $number2->getImaginary() + $number1->getImaginary() * $number2->getReal();
        return new ComplexNumber($r, $y);
    }

    /**
     * Divides two complex numbers
     *
     * @param ComplexNumber $number1
     * @param ComplexNumber $number2
     * @return ComplexNumber
     */
    public static function div(ComplexNumber $number1, ComplexNumber $number2): ComplexNumber
    {
        $div = $number2->getReal() * $number2->getReal() + $number2->getImaginary() * $number2->getImaginary();
        $r = $number1->getReal() * $number2->getReal() + $number1->getImaginary() * $number2->getImaginary();
        $r = $r/$div;
        $y = $number1->getImaginary() * $number2->getReal() - $number1->getReal() * $number2->getImaginary();
        $y = $y/$div;
        return new ComplexNumber($r, $y);
    }
}