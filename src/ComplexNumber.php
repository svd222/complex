<?php
namespace svd\complex;

/**
 * Class ComplexNumber represents human-readable form of a complex number
 *
 * @package svd\complex
 */
class ComplexNumber
{
    private float $real;

    private float $imaginary;

    public function __construct(float $real, float $imaginary)
    {
        $this->real = $real;
        $this->imaginary = $imaginary;
    }

    public function getReal(): float
    {
        return $this->real;
    }

    public function getImaginary(): float
    {
        return $this->imaginary;
    }

    public function __toString()
    {
        return $this->real . " + " . $this->imaginary . "i";
    }
}