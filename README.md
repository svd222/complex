Test application implements the simplest operations on complex numbers:
addition, multiplication, subtraction, division.

###INSTALLATION & INITIALIZATION
```
$ git clone https://svd222@bitbucket.org/svd222/complex.git
$ composer update
```

###RUNNING TESTS
```
$ ./vendor/bin/codecept run
```